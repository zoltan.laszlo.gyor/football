package player;
import java.lang.StringBuilder;
import player.Position;

public class Player{
  private String firstName;
  private String secondName;
  private String nickName;
  private int kitNumber;
  private double height;
  private int value;
  private int goalsScroed;
  private int assists;
  private int yellowCards;
  private int redCards;
  private int gamesPlayed;
  private String team;
  private int age;
  private Position pos;

  public Player(String firstName, String secondName, String nickName, int kitNumber, double height, int value, int goalsScroed,
    int assists, int yellowCards, int redCards, int gamesPlayed, String team, int age, Position pos){
      this.firstName = firstName;
      this.secondName = secondName;
      this.nickName = nickName;
      this.kitNumber = kitNumber;
      this.height = height;
      this.value = value;
      this.goalsScroed = goalsScroed;
      this.assists = assists;
      this.yellowCards = yellowCards;
      this.redCards = redCards;
      this.gamesPlayed = gamesPlayed;
      this.team = team;
      this.age = age;
      this.pos = pos;
    }

    public Player(String firstName, String secondName, double height, int kitNumber, int age){
      this.firstName = firstName;
      this.secondName = secondName;
      this.kitNumber = kitNumber;
      this.height = height;
      this.age = age;
    }

  public String getFirstName(){
    return this.firstName;
  }

  public String getSecondName(){
    return this.secondName;
  }

  public String getWholeName(){
    return this.firstName + " " + this.secondName;
  }

  public int getkitNumber(){
    return this.kitNumber;
  }

  public double getHeight(){
    return this.height;
  }

  public int getValue(){
    return this.value;
  }

  public int getGoalsScroed(){
    return this.goalsScroed;
  }

  public int getAssists(){
    return this.assists;
  }

  public int getYellowCards(){
    return this.yellowCards;
  }

  public int getRedCards(){
    return this.redCards;
  }

  public int getGamesPlayed(){
    return this.gamesPlayed;
  }

  public String getTeam(){
    return this.team;
  }

  public int getAge(){
    return this.age;
  }

  public Position getPos(){
    return this.pos;
  }

  public void scoreGoal(){
    this.goalsScroed += 1;
  }

  public void scoreGoal(int x){
    this.goalsScroed += x;
  }


  public String toString(){ //nagym�ret� sz�vegn�l ez t kell haszn�lni
    StringBuilder sb = new StringBuilder();
    sb.append("First name: ");
    sb.append(firstName + "\r\n");
    sb.append("Second name: ");
    sb.append(secondName + "\r\n");
    sb.append("Nickname: ");
    sb.append(nickName + "\r\n");
    sb.append("Kit number: ");
    sb.append(kitNumber + "\r\n");
    sb.append("Value : ");
    sb.append(value + "\r\n");
    sb.append("Number of goals scored: ");
    sb.append(goalsScroed + "\r\n");
    sb.append("Assists: ");
    sb.append(assists + "\r\n");
    sb.append("Number of yellow cards: ");
    sb.append(yellowCards + "\r\n");
    sb.append("Number of red cards: ");
    sb.append(redCards + "\r\n");
    sb.append("Number of games played: ");
    sb.append(gamesPlayed + "\r\n");
    sb.append("Current team: ");
    sb.append(team + "\r\n");
    sb.append("Age: ");
    sb.append(age + "\r\n");
    sb.append("Position: ");
    sb.append(pos + "\r\n");
    String finalText = sb.toString();
    return finalText;
  }
}
