package team;
import java.lang.StringBuilder;
import player.Player;

public class Team{
  private String name;
  private int yearClubWasFound;
  private String nickName;
  private String coach;
  private String stadium;
  private int stadiumCap;
  private Player[] goalKeepers;
  private Player[] defenders;
  private Player[] midfielders;
  private Player[] attackers;

  public Team(String name, int yearClubWasFound, String nickName, String coach, String stadium,
   int stadiumCap, Player[] goalKeepers, Player[] defenders, Player[] midfielders, Player[] attackers){
     this.name = name;
     this.yearClubWasFound = yearClubWasFound;
     this.nickName = nickName;
     this.coach = coach;
     this.stadium = stadium;
     this.stadiumCap = stadiumCap;
     this.goalKeepers = goalKeepers;
     this.defenders = defenders;
     this.midfielders = midfielders;
     this.attackers = attackers;
   }

   public String getName(){
     return this.name;
   }

   public int getYearClubWasFound(){
     return this.yearClubWasFound;
   }

   public String getNickName(){
     return this.nickName;
   }

   public String getCoach(){
     return this.coach;
   }

   public String getStadium(){
     return this.stadium;
   }

   public int getStadiumCap(){
     return this.stadiumCap;
   }

   public Player[] getGoalKeepers(){
     return this.goalKeepers;
   }

   public Player[] getDefenders(){
     return this.defenders;
   }

   public Player[] getMidfielders(){
     return this.midfielders;
   }

   public Player[] getAttackers(){
     return this.attackers;
   }

   public String toString(){
     StringBuilder sb = new StringBuilder();
     sb.append("Goalkeepers: \r\n");
     for(int i = 0; i<goalKeepers.length; i++){
       sb.append(goalKeepers[i].getWholeName());
       sb.append(" ");
     }
     sb.append("\r\n");
     sb.append("Defenders: \r\n");
     for(Player actualPlayer : defenders){ //v�ltoz� t�pusa + valtoz� neve + t�mb neve->csak t�mb eset�n lehets�ges!!!
       sb.append(actualPlayer.getWholeName());
       sb.append(" ");
     }
     sb.append("\r\n");
     sb.append("Midfielders: \r\n");
     for(Player actualPlayer : midfielders){
       sb.append(actualPlayer.getWholeName());
       sb.append(" ");
     }
     sb.append(" ");
     sb.append("Attackers: \r\n");
     for(Player actualPlayer : attackers){
       sb.append(actualPlayer.getWholeName());
       sb.append(" ");
     }
     String finalText = sb.toString();
     return finalText;
   }

}