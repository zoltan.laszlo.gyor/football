import player.Player;
import player.Position;
import team.Team;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.lang.NumberFormatException;
import java.io.PrintWriter;
import java.io.IOException;

public class Main{
  public static void main(String[] args){
    Player[] players = new Player[4];
  try{
    File file = new File ("players.txt");
    Scanner sc = new Scanner(file);
    File output = new File("output.txt");
    PrintWriter pw = new PrintWriter(output);
    int i = 0;
    while(sc.hasNextLine()){
      String line = sc.nextLine();
      String[] details = line.split(","); //Feldarabolja a sz�veget a ()-be �rt alapja�n;
      int kitNumber = Integer.parseInt(details[3]);//Sz�mb�l sz�vegg� alak�t�s
      double height = Double.parseDouble(details[4]);
      int value = Integer.parseInt(details[5]);
      int goalsScroed = Integer.parseInt(details[6]);
      int assists = Integer.parseInt(details[7]);
      int yellowCards = Integer.parseInt(details[8]);
      int redCards = Integer.parseInt(details[9]);
      int gamesPlayed = Integer.parseInt(details[10]);
      int age = Integer.parseInt(details[12]);
      Position pos;
      if(details[13].equals("STRIKER")){ //param�terenk�nt n�zi meg
        pos = Position.STRIKER;          // a == egyenl�s�g csak prim�vekn�l(int, char, boolean, byte, double, float) elemekn�l kell vizsg�lni
      }else if(details[13].equals("MIDFIELDER")){
        pos = Position.MIDFIELDER;
      }else if(details[13].equals("DEFENDER")){
        pos = Position.DEFENDER;
      }else{
        pos = Position.GOALKEEPER;
      }
      players[i] = new Player(details[0],details[1],details[2],kitNumber,height,value,goalsScroed,assists,yellowCards,redCards,gamesPlayed,details[11],age,pos);
      pw.println(players[i].toString());
      i++;
    }
    pw.close();
  }catch(FileNotFoundException e){
      System.out.println("Hiba a sz�veg beolvas�s�val");
  }catch(NumberFormatException e){
    System.out.println("Hiba a sz�mm� val� kovert�l�sn�l");
  }
  System.out.println(players[0].toString());
  }
}
